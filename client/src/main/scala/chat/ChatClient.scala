package chat

import chat.prot.ChatMessage

object ChatClient extends App {
  import org.scalajs.dom.{document, window}
  import org.scalajs.dom.raw.{CloseEvent, Event, MessageEvent, MouseEvent, WebSocket}

  val wsEndpoint = "ws://127.0.0.1:8080/subscribe"
  val ws = new WebSocket(wsEndpoint)

  document.body.appendChild(SiteTemplate.renderSiteTemplate)
  SiteTemplate.sendbutton.onclick = {
    case _: MouseEvent =>
      val msg = SiteTemplate.msgTextbox.value
      val author = SiteTemplate.usernameTextbox.value
      val chatMessage = ChatMessage(author, msg)
      ws.send(ChatMessage.serialize(chatMessage))
      SiteTemplate.msgTextbox.value = ""
    case _ =>
  }

  ws.onmessage = {
    case me: MessageEvent =>
      val msg = ChatMessage.deserialize(me.data.asInstanceOf[String])
      val rendered = SiteTemplate.renderChatLine(msg.author, msg.text)
      SiteTemplate.chattable.appendChild(rendered)
    case _ =>
  }
  ws.onerror = {
    case event: Event => println(event)
    case _ =>
  }
  ws.onopen = {
    case event: Event => println(event)
    case _ =>
  }
  ws.onclose = {
    case event: CloseEvent => println(event)
    case _ =>
  }
  window.onbeforeunload = {
    case _ => ws.close()
  }
}

object SiteTemplate {
  import org.scalajs.dom.{document}
  import org.scalajs.dom.html.{Div, Form, Table, TableRow}
  import org.scalajs.dom.raw.{HTMLButtonElement, HTMLInputElement, HTMLTableElement}
  import scalatags.JsDom.all._

  private val ID_CONTENT: String = "content"
  private val ID_HEADER: String = "header"
  private val ID_CHATTABLE: String = "chattable"
  private val ID_USERNAMETEXTBOX: String = "usertxtbx"
  private val ID_MSGTEXTBOX: String = "msgtxtbx"
  private val ID_SENDBUTTON: String = "sendbtn"
  private val CLASS_CHATLINE: String = "chatline"
  private val CLASS_CHATAUTHOR: String = "author"
  private val CLASS_CHATTEXT: String = "chattxt"

  private val CHAT_HEADER: String = "Chat"
  private val TABLE_HEAD_USER: String = "user"
  private val TABLE_HEAD_MESSAGE: String = "message"
  private val SENDBUTTON_VALUE: String = "send"

  def renderHeader: Div = div(id:=ID_HEADER, h1(CHAT_HEADER)).render
  def renderChattable: Table = table(id:=ID_CHATTABLE, th(TABLE_HEAD_USER), th(TABLE_HEAD_MESSAGE)).render
  def renderSendform: Form = form(
    input(`type`:="textbox", id:=ID_USERNAMETEXTBOX),
    input(`type`:="textbox", id:=ID_MSGTEXTBOX),
    input(id:=ID_SENDBUTTON, `type`:="button", value:=SENDBUTTON_VALUE)
  ).render
  def renderChatLine(author: String, text: String): TableRow =
    tr(`class`:=CLASS_CHATLINE, td(`class`:=CLASS_CHATAUTHOR, author), td(`class`:=CLASS_CHATTEXT, text)).render
  def renderSiteTemplate: Div = div(id:=ID_CONTENT, renderHeader, renderChattable, renderSendform).render

  def usernameTextbox = byId[HTMLInputElement](ID_USERNAMETEXTBOX)
  def msgTextbox = byId[HTMLInputElement](ID_MSGTEXTBOX)
  def sendbutton = byId[HTMLButtonElement](ID_SENDBUTTON)
  def chattable = byId[HTMLTableElement](ID_CHATTABLE)

  private def byId[T](id: String): T = document.getElementById(id).asInstanceOf[T]
}