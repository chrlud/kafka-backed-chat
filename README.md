# Kafka Backed Chat

Simple server and SPA application using Akka-Http and Scala.js with Apache Kafka Backend

This is a just a simple POC for a simple Chat server and client using Scala.js and websocket for the client, Akka-Http, Apache Kafka and Alpakka Kafka for the backend.

### Build

Client:
```shell script
$ sbt clientJS/fastOptJS::webpack
...
[info]                        Asset      Size                         Chunks
[info]     client-fastopt-bundle.js   1509534   [emitted]   [client-fastopt]
[info] client-fastopt-bundle.js.map    780033   [emitted]   [client-fastopt]
```

Server:
```shell script
$ sbt server/package
```

### Configuration

Configuration file can be found under `server/src/main/resources/application.conf`.
It controls several parameters for the server like hostname and port and tells the server where to find the kafka server and which topic to subscribe to. 

### Running

Running requires having a running kafka server with configured topic.

```shell script
$ sbt server/run
...
[info] running chat.Chat 
Server online at http://localhost:8080/
Press RETURN to stop...
```

### Licencing

MIT license (MIT, https://opensource.org/licenses/MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.