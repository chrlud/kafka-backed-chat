package chat.prot

case class ChatMessage(author: String, text: String)

object ChatMessage {
  import io.bullet.borer.Json
  import io.bullet.borer.derivation.MapBasedCodecs._

  implicit val chatMessageCodec = deriveCodec[ChatMessage]

  def deserialize(chatMessageString: String): ChatMessage = {
    val bytes = chatMessageString.getBytes("UTF8")
    Json.decode(bytes).to[ChatMessage].value
  }

  def serialize(chatMessage: ChatMessage): String =
    Json.encode(chatMessage).toUtf8String
}