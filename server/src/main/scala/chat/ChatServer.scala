package chat

import akka.{Done, NotUsed}
import akka.actor.ActorSystem
import akka.http.scaladsl.{Http, server}
import akka.http.scaladsl.model.ws.{Message, TextMessage}
import akka.http.scaladsl.server.Directives._
import akka.kafka.{ConsumerSettings, ProducerSettings, Subscriptions}
import akka.kafka.scaladsl.{Consumer, Producer}
import akka.stream.Materializer
import akka.stream.scaladsl.{BroadcastHub, Flow, Sink, Source}
import org.apache.kafka.clients.consumer.{ConsumerConfig, ConsumerRecord}
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}

import scala.concurrent.Future
import scala.util.Random

case class HttpConfig(assetsDirectory: String, host: String, port: Int)
case class KafkaConfig(bootstrapServers: String, topics: String)

class ChatServer(httpConfig: HttpConfig, kafkaConfig: KafkaConfig) {

  implicit val system: ActorSystem = ActorSystem("chatserver-system")
  implicit val materializer: Materializer = Materializer(system)

  val flowGenerator = new KafkaFlow(kafkaConfig)

  var bindingFuture: Future[Http.ServerBinding] = _

  def run() {
    val route = createRoute()
    bindingFuture = Http().newServerAt(httpConfig.host, httpConfig.port).bind(route)
  }

  def kill(): Unit = {
    import system.dispatcher
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done
  }

  val r = Random
  def createRandomClientId = s"Client_${r.nextInt(10000)}"

  def createRoute(): server.Route = {
    pathEndOrSingleSlash {
      get {
        getFromResource("chat.html")
      }
    } ~
      pathPrefix("assets") {
        get {
          getFromDirectory(httpConfig.assetsDirectory)
        }
      } ~
      path("subscribe") {
        handleWebSocketMessages(flowGenerator.flow(createRandomClientId))
      }
  }
}

class KafkaFlow(kafkaConfig: KafkaConfig)(implicit system: ActorSystem, materializer: Materializer) {

  def flow(clientId: String): Flow[Message, Message, NotUsed] = {
    Flow.fromSinkAndSource(kafkaChatSink, kafkaChatSource(clientId))
  }

  // TODO: try if it works for multiple consumers, otherwise -> def
  val baseConsumerSettings: ConsumerSettings[String, String] =
    ConsumerSettings.create(system, new StringDeserializer, new StringDeserializer)
    .withBootstrapServers(kafkaConfig.bootstrapServers)
    .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")

  def kafkaChatSource(groupId: String): Source[Message, NotUsed] = {
    Consumer.plainSource(baseConsumerSettings.withGroupId(groupId), Subscriptions.topics(kafkaConfig.topics))
      .map { _.value }
      .runWith(BroadcastHub.sink(1024))
      .map { TextMessage(_) }
  }

  def kafkaChatSink: Sink[Message, Future[Done]] = {
    Sink.foreach { msg: Message =>
      msg.asTextMessage.getStreamedText.runWith(
        Sink.foreach { txtMsg => writeToKafka(txtMsg) }, materializer)
    }
  }

  // TODO: Same - try, otherwise def
  val producerSettings: ProducerSettings[String, String] = {
    ProducerSettings.create(system, new StringSerializer, new StringSerializer)
      .withBootstrapServers(kafkaConfig.bootstrapServers)
  }

  def writeToKafka(msg: String): Future[Done] = {
    Source.single(new ProducerRecord[String, String](kafkaConfig.topics, msg))
      .runWith(Producer.plainSink(producerSettings))
  }

}
