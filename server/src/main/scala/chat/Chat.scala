package chat

import com.typesafe.config.ConfigFactory

import scala.io.StdIn

object Chat extends App {
  val (httpConfig, kafkaConfig) = loadConfig

  val chatServer = new ChatServer(httpConfig, kafkaConfig)
  chatServer.run
  println(s"Server online at http://${httpConfig.host}:${httpConfig.port}/\nPress RETURN to stop...")
  StdIn.readLine
  chatServer.kill

  def loadConfig: (HttpConfig, KafkaConfig) = {
    val appConfig = ConfigFactory.load("application.conf")
      .withFallback(ConfigFactory.load("base.conf"))
    (
      HttpConfig(
        appConfig.getString("chatserver.assetsDirectory"),
        appConfig.getString("chatserver.host"),
        appConfig.getInt("chatserver.port")),
      KafkaConfig(
        appConfig.getString("chatkafka.bootstrapServers"),
        appConfig.getString("chatkafka.topics"))
    )
  }
}