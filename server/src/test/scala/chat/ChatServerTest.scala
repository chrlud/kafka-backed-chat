package chat

import akka.http.scaladsl.testkit.WSProbe
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.http.scaladsl.server.Directives._
import chat.prot.ChatMessage
import org.scalatest.flatspec.AnyFlatSpec
import org.testcontainers.containers.KafkaContainer

class ChatServerTest extends AnyFlatSpec with ScalatestRouteTest {
  "An empty Set" should "have size 0" in {
    assert(Set.empty.size == 0)
  }

  "The websocket route" should "route a message to all listeners" in {
    val kafka = new KafkaContainer()
    kafka.start()
    val bsServers = kafka.getBootstrapServers
    val kafkaConfig = KafkaConfig(bsServers, "testtopic")
    val kafkaFlow = new KafkaFlow(kafkaConfig)
    def wsRoute(clientId: String) = path("subscribe"){
      handleWebSocketMessages(kafkaFlow.flow(clientId))
    }

    val msg1 = ChatMessage("Adam", "Message 1")
    val msg2 = ChatMessage("Bert", "Message 2")

    val wsClient1 = WSProbe()
    WS("/subscribe", wsClient1.flow) ~> wsRoute("Adam") ~>
      check {
        wsClient1.sendMessage(ChatMessage.serialize(msg1))
        wsClient1.expectMessage(ChatMessage.serialize(msg1))
      }

    val wsClient2 = WSProbe()
    WS("/subscribe", wsClient2.flow) ~> wsRoute("Bert") ~>
      check {
        wsClient2.sendMessage(ChatMessage.serialize(msg2))
        wsClient2.expectMessage(ChatMessage.serialize(msg1))
        wsClient2.expectMessage(ChatMessage.serialize(msg2))
      }

    kafka.stop()
  }
}
