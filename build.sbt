ThisBuild / name := "kafka_chat"
ThisBuild / version := "1.0"
ThisBuild / scalaVersion := "2.13.1"

val AkkaVersion = "2.6.8"
val AkkaHttpVersion = "10.2.0"
val AkkaStreamKafkaVersion = "2.0.4"
val KafkaVersion = "2.6.0"
val NScalaTimeVersion = "2.24.0"
val ScalatagsVersion = "0.9.1"
val ScalaJSDomVersion = "1.0.0"
val BorerVersion = "1.6.1"
val TSConfigVersion = "1.4.0"
val ScalaTestVersion = "3.2.0"
val KafkaTestcontainersVersion = "1.14.3"

lazy val shared = crossProject(JSPlatform, JVMPlatform)
  .crossType(CrossType.Pure)
  .settings(
    scalaVersion := "2.13.1",
    libraryDependencies ++= Seq(
      "io.bullet" %%% "borer-core"       % BorerVersion,
      "io.bullet" %%% "borer-derivation" % BorerVersion
    )
  )
  .in(file("shared"))
  .jsConfigure(_.enablePlugins(ScalaJSBundlerPlugin))

lazy val sharedJS: Project = shared.js
lazy val sharedJVM = shared.jvm


lazy val client = crossProject(JSPlatform)
  .crossType(CrossType.Pure)
  .in(file("client"))
  .jsConfigure(_.enablePlugins(ScalaJSBundlerPlugin))
  .jsSettings(
    libraryDependencies ++= Seq(
      "org.scala-js" %%% "scalajs-dom" % ScalaJSDomVersion,
      "com.lihaoyi"  %%% "scalatags"   % ScalatagsVersion,
    ),
    scalaJSUseMainModuleInitializer := true,
  )

lazy val clientJs = client.js
  .dependsOn(sharedJS)

lazy val server = (project in file("server"))
    .settings(
      libraryDependencies ++= Seq(
        "com.typesafe.akka"      %% "akka-stream-kafka"   % AkkaStreamKafkaVersion,
        "com.typesafe.akka"      %% "akka-stream"         % AkkaVersion,
        "com.typesafe.akka"      %% "akka-http"           % AkkaHttpVersion,
        "org.apache.kafka"       %% "kafka"               % KafkaVersion,
        "org.apache.kafka"       %% "kafka-streams-scala" % KafkaVersion,
        "com.github.nscala-time" %% "nscala-time"         % NScalaTimeVersion,
        "com.lihaoyi"            %% "scalatags"           % ScalatagsVersion,
        "com.typesafe"            % "config"              % TSConfigVersion,
        "org.scalatest"          %% "scalatest"           % ScalaTestVersion            % "test",
        "com.typesafe.akka"      %% "akka-stream-testkit" % AkkaVersion                 % "test",
        "com.typesafe.akka"      %% "akka-http-testkit"   % AkkaHttpVersion             % "test",
        "org.testcontainers"      % "kafka"               % KafkaTestcontainersVersion  % "test"
      ),
      Compile / resourceGenerators += Def.task {
        val configFile = (Compile / resourceManaged).value / "base.conf"
        val jsOutputDir = clientJs.base / "target" / "scala-2.13" / "scalajs-bundler" / "main"
        val content =
          s"""
             |chatserver {
             |  assetsDirectory: ${jsOutputDir.getCanonicalPath}
             |}""".stripMargin
        IO.write(configFile, content)
        Seq(configFile)
      }.taskValue
    )
  .dependsOn(sharedJVM)